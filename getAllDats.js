const fs = require('fs')
const path = require('path')

module.exports = function (dir) {
  var rootDir = dir || process.cwd()
  rootDir = path.normalize(rootDir) + '/'
  return fs.readdirSync(rootDir)
    .map((t) => rootDir + t)
    .filter(isDir)
    .filter(isDatDir)
    .map(getPublicKeyFromDatDir)
}

function getPublicKeyFromDatDir (dir) {
  return { key: `dat://${fs.readFileSync(`${dir}/.dat/metadata.key`).toString('hex')}`, dir: dir }
}

function isDir (thing) {
  return fs.lstatSync(thing).isDirectory()
}

function isDatDir (dir) {
  return fs.readdirSync(dir).indexOf('.dat') !== -1
}
