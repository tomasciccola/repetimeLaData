#!/usr/bin/node

const path = require('path')
const Dat = require('dat-node')
const diffy = require('diffy')({ fullscreen: true })
const trim = require('diffy/trim+newline')
const input = require('diffy/input')()
const Menu = require('menu-string')
const getDats = require('./getAllDats.js')
const c = require('ansi-colors')
const clipboard = require('clipboardy')
const stripAnsi = require('strip-ansi')

var state = { dats: [], msg: '', selectedDatKey: '' }
var datsToShare = getDats(process.argv[2])

if (datsToShare.length === 0) {
  console.log('no dats on cwd or path arg, exiting')
  process.exit(1)
} else {
  datsToShare.map(connectDat)
}

function connectDat (d) {
  Dat(d.dir, function (err, dat) {
    if (err)console.log(err)
    if (dat.writable)dat.importFiles()
    dat.peers = 0
    state.dats.push(dat)
    diffy.render()
    dat.joinNetwork()

    dat.network.on('connection', function () {
      diffy.render()
    })

    dat.network.on('connection-closed', function () {
      diffy.render()
    })
  })
}

var menu = new Menu({
  items: datsToShare,
  render: (item, sel) => item.text,
  height: 10 })

diffy.render(mainView)

function mainView () {
  menu.items = state.dats.map(renderDat)
  return trim(`
  ${header()}
  ${menu.toString()}
  ${'\n' + c.red('key--> ') + state.selectedDatKey}
  ${state.msg}
  `)

  function renderDat (dat, i) {
    var s = ''
    var separator = ''

    var nPeers = dat.network
      ? c.cyan(' (peers: ' + dat.network.connections.length + ')')
      : c.red('✗')
    var dirName = ''
    if (menu._selected === i) {
      dirName = `${c.red(path.basename(dat.path))}`
      state.selectedDatKey = `${c.magenta('dat://' + dat.key.toString('hex'))}`
    } else {
      dirName = `${c.green(path.basename(dat.path))}`
    }

    var strSize = dirName.length + nPeers.length

    for (let i = strSize; i < diffy.width; i++) {
      separator += ' '
    }
    s += dirName
    s += nPeers
    s += separator
    return { text: s }
  }
}

function header () {
  return (`${c.magenta('~~~ repetime la data ~~~')}
  seedea dats de una carpeta que le pases
`)
}

diffy.on('resize', function () {
  diffy.render(mainView)
})

input.on('up', function () {
  menu.up()
  diffy.render()
})

input.on('down', function () {
  menu.down()
  diffy.render()
})

input.on('enter', function () {
  clipboard.writeSync(stripAnsi(state.selectedDatKey))
  state.msg = 'copied to clipboard!!'
  setTimeout(() => {
    state.msg = ''
    diffy.render()
  }, 3000)
  diffy.render()
})

input.on('keypress', function (k) {
  switch (k) {
    case 'j':
      menu.down()
      break
    case 'k':
      menu.up()
      break
  }
  diffy.render()
})

menu.on('update', function () {
  diffy.render()
})
