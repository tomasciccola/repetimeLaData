# Repetime la data

Simple dat cli for seeding multiple files

## Usage

* Clone the repo
* npm install
* node app.js "path"

It will seed all your archives located at the path you pass (and show you the number of seeds)
You can move with arrows or j/k to see the public key. If you press enter on the selected key it will copy it to the clipboard

I've also published in the ssb-npm registry, so if you follow me you can do 
ssb-npm install repetime-la-data
